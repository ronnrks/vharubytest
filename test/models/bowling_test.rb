require 'test_helper'

class BowlingTest < ActiveSupport::TestCase
  def setup
    @b = Bowling.new
  end

  def play_same_shot number_of_times, pins
  	number_of_times.times do 
  		@b.play pins	
  	end	
  end

  def all_spare_game_of_five_pins
  	play_same_shot 21, 5	
  end
  
  test "should have blank Bowling " do 
  	assert_instance_of(Bowling, @b)
  end

  test "should check whether frames is an array " do
  	assert_equal @b.frames.class, Array
  end

  test "should check whether shots is an array " do
  	assert_equal @b.frames.class, Array
  end

  test "user can play bowling with pins" do
  	assert(@b.play 0)
  end

  test "should have some shots" do
  	@b.play 0
  	@b.play 1

  	assert(@b.shots.length > 0)	
  end

  test "should have score method" do
  	assert(@b.score)
  end

  test "played for all zero game" do
  	play_same_shot 10, 0

  	assert_equal(@b.score , 0)
  end

  test "played game for score 10" do
  	@b.play 1
	@b.play 2
	@b.play 3
	@b.play 4

	assert_equal(@b.score , 10)	
  end

  test "played game for score 29" do
  	@b.play 9
	@b.play 1
	@b.play 9
	@b.play 1

	assert_equal(@b.score , 29)	
  end

  test "played game for 18" do
  	play_same_shot 4, 1
	@b.play 10
	play_same_shot 2, 1

	assert_equal(@b.score , 18)	
  end

  test "plyed a perfect game" do
  	play_same_shot 12, 10

  	assert_equal(@b.score , 300)	
  end

  test "played for all 5 pins game" do
  	all_spare_game_of_five_pins

  	assert_equal(@b.score , 150)
  end

  test "plays full spare game for score 156" do
  	@b.play 3
	@b.play 7
	@b.play 8
	@b.play 2
	@b.play 6
	@b.play 4
	@b.play 8
	@b.play 2
	@b.play 4
	@b.play 6
	play_same_shot 11, 5

	assert_equal(@b.score , 156)
  end

  test "plays full spare game for score 188" do
  	@b.play 10
	@b.play 10
	@b.play 6
	@b.play 2
	@b.play 7
	@b.play 1
	@b.play 2
	@b.play 8
	@b.play 9
	@b.play 1
	@b.play 3
	@b.play 7
	play_same_shot 3, 10
	@b.play 6
	@b.play 4

	assert_equal(@b.score , 188)
  end

  test "plays full spare game for score 165" do
  	@b.play 6
	@b.play 4
	@b.play 8
	@b.play 2
	@b.play 4
	@b.play 6
	@b.play 5
	@b.play 5
	@b.play 2
	@b.play 8
	@b.play 6
	@b.play 4
	@b.play 2
	@b.play 8
	@b.play 9
	@b.play 1
	@b.play 0
	@b.play 10
	@b.play 10
	@b.play 10
	@b.play 9

	assert_equal(@b.score , 165)
  end

  test "plays full spare game for score 146" do
  	@b.play 6
	@b.play 4
	@b.play 8
	@b.play 2
	@b.play 4
	@b.play 6
	@b.play 5
	@b.play 5
	@b.play 2
	@b.play 8
	@b.play 6
	@b.play 4
	@b.play 2
	@b.play 8
	@b.play 9
	@b.play 1
	@b.play 0
	@b.play 10
	@b.play 0
	@b.play 10
	@b.play 10

	assert_equal(@b.score , 146)
  end

  test "plays full spare game for score 155" do
  	@b.play 6
	@b.play 4
	@b.play 8
	@b.play 2
	@b.play 4
	@b.play 6
	@b.play 5
	@b.play 5
	@b.play 2
	@b.play 8
	@b.play 6
	@b.play 4
	@b.play 2
	@b.play 8
	@b.play 9
	@b.play 1
	@b.play 0
	@b.play 10
	@b.play 10
	@b.play 6
	@b.play 3

	assert_equal(@b.score , 155)
  end
end

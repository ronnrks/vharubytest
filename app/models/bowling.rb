class Bowling < ActiveRecord::Base
	attr_accessor :frames, :shots

	# Maximum score of a single frame
	MAX_SCORE_SINGLE_FRAME = 10
	# Maximum allowed frames in a game
	MAX_FRAME_SIZE = 12
	# Maximum index to calculate score
	MAX_INDEX_TO_CALCULATE_SCORE = 10

	# Initializing frames and shots as an array
	def initialize
		@frames = []
		@shots = []
	end 

	def play pins
		@shots.push pins
	end

	# Calculate total score of a game
	def score
		prepare_frames

		total_score = 0
    	current_frame_index = 0

		MAX_INDEX_TO_CALCULATE_SCORE.times do 
			if strike? current_frame_index
				total_score += strike_bonus current_frame_index	
			elsif spare? current_frame_index
				total_score += spare_bonus current_frame_index	
			else
				total_score += frame_score current_frame_index
			end
			current_frame_index += 1
		end
		total_score
	end
  
  private
  	# Preparing all frames before calculation of score. 
  	# This is not required if player plays the full game. 
  	# But here we have to calculate score of a unfinished game also.
  	def prepare_frames
		i = 0
		while @frames.length != MAX_FRAME_SIZE
			if @shots[i] == MAX_SCORE_SINGLE_FRAME || @frames.length >= MAX_INDEX_TO_CALCULATE_SCORE
				@frames.push [@shots[i].to_i]
				i += 1
			else
				@frames.push [@shots[i].to_i, @shots[i + 1].to_i]
				i += 2
			end	
		end	
	end

	# Check whether current frame has strike ?
  	def strike? current_frame_index
  		@frames[current_frame_index][0] == MAX_SCORE_SINGLE_FRAME
  	end

  	# Calculate bonus score for a strike frame
  	def strike_bonus current_frame_index
  		if @frames[current_frame_index+1][0] == MAX_SCORE_SINGLE_FRAME || current_frame_index == MAX_INDEX_TO_CALCULATE_SCORE - 1
			MAX_SCORE_SINGLE_FRAME + @frames[current_frame_index+1][0] + @frames[current_frame_index + 2][0] 
		else	
			MAX_SCORE_SINGLE_FRAME + frame_score(current_frame_index + 1)
		end	
  	end

  	# Check whether current frame has spare ?
  	def spare? current_frame_index
  		frame_score(current_frame_index) == MAX_SCORE_SINGLE_FRAME
  	end

  	# Calculate bonus score for a spare frame
  	def spare_bonus current_frame_index
  		MAX_SCORE_SINGLE_FRAME + @frames[current_frame_index+1][0]	
  	end

  	# Calulate score of a frame 
  	def frame_score current_frame_index
  		@frames[current_frame_index].inject(:+)
  	end
end
